﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace Discounts.Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            IWebDriver driver = new ChromeDriver(); 
            driver.Navigate().GoToUrl("http://localhost:56698/");

            IWebElement createOffer = driver.FindElement(By.Id("AddButton"));
            createOffer.Click();

            IWebElement requesType = driver.FindElement(By.Id("48"));
            requesType.Click();

            IWebElement chooseClientBtn = driver.FindElement(By.Id("ChooseClient"));
            chooseClientBtn.Click();
            
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));

            //IWebElement clietName = driver.FindElement(By.Id("FullName"));
            IWebElement clietName = driver.FindElement(By.ClassName("FullName"));
            clietName.Click();
            clietName.SendKeys("Alfa");

            IList<IWebElement> clientRows = driver.FindElements(By.ClassName("grid-row "));
            clientRows.First().Click();

        }
    }
}
